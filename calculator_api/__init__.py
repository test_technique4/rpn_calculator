"""A wonderful API doing wonderful things built with FastAPI."""
from fastapi import FastAPI
from calculator_api.routes.calculation import router as calculation_router
from calculator_api.dependency_injection import Container


app: FastAPI = FastAPI()

container = Container()
app.container = container

app.include_router(calculation_router)
