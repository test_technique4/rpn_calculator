import mysql.connector.pooling
from mysql.connector import pooling
from typing import AsyncGenerator
from databases import Database
from dependency_injector import containers, providers
from calculator_api.constants import DB_POOL_TIMEOUT, DATABASE_POOL_SIZE
from calculator_api.services.calculations import CalculationServices
from calculator_api.repository.calculations import CalculationRepository


async def init_database_pool(url: str, recycle: int) -> AsyncGenerator[Database, None]:
    """Initialize the database"""
    db_config = {
        'pool_size': DATABASE_POOL_SIZE,
        'pool_reset_session': recycle,
        'user': 'test',
        'password': 'test',
        'host': 'db',
        'port': 3306,
        'database': 'your_database',
    }

    db_pool = pooling.MySQLConnectionPool(**db_config)
    my_connection = db_pool.get_connection()
    yield my_connection
    my_connection.close()


class Container(containers.DeclarativeContainer):
    """Dependency injection container."""

    # Wiring container
    wiring_config = containers.WiringConfiguration(modules=["calculator_api.routes.calculation"])

    # Inject configuration
    app_config = providers.Configuration()

    # Initialize database
    final_db_url = 'mysql://test:test@db:3306/your_database'

    db_pool = providers.Resource(init_database_pool, url="final_db_url", recycle=DB_POOL_TIMEOUT)

    # Inject repositories
    calculation_repository: providers.Factory[CalculationRepository] = \
        providers.Factory(CalculationRepository, db=db_pool.provided.connection.call())
    calculation_service: providers.Factory[CalculationServices] = \
        providers.Factory(CalculationServices, repository=calculation_repository)
