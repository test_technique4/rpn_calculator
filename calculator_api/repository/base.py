from abc import ABC, abstractmethod
from typing import Optional
from calculator_api.models.calculation_models import AllCalculations, CalculationSolved


class AbstractCalculationRepository(ABC):
    """Calculation repository interface"""

    @staticmethod
    @abstractmethod
    async def insert(calculation_solved: CalculationSolved):
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    async def list() -> Optional[AllCalculations]:
        raise NotImplementedError
