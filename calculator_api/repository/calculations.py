from databases.core import Connection
from typing import Optional
from pypika import Table, Query
from pydantic import TypeAdapter
from calculator_api.models.calculation_models import CalculationSolved, AllCalculations
from calculator_api.repository.base import AbstractCalculationRepository
from calculator_api.exceptions import APIException


# def create_connection():
#     return mysql.connector.connect(**DB_CONFIG)
#
#
# def close_connection(connection):
#     connection.close()
#
#
# async def execute_query(query, values=None):
#     connection = create_connection()
#     cursor = connection.cursor()
#
#     try:
#         cursor.execute(query, values)
#         connection.commit()
#     except Exception as e:
#         connection.rollback()
#         raise e
#     finally:
#         cursor.close()
#         close_connection(connection)
#
#
# async def fetch_all(query):
#     connection = create_connection()
#     cursor = connection.cursor(dictionary=True)
#     rows = {}
#
#     try:
#         cursor.execute(str(query))
#         rows = cursor.fetchall()
#     except Exception as e:
#         connection.rollback()
#         raise e
#     finally:
#         cursor.close()
#         close_connection(connection)
#         return rows


class CalculationRepository(AbstractCalculationRepository):
    """Calculation repository"""

    def __init__(self, db: Connection):
        self.db = db

    async def insert(self, calculation_solved: CalculationSolved):
        try:
            calculation_results = Table('calculation_results')
            query = Query.into(calculation_results).columns(calculation_results.expression, calculation_results.result)\
                .insert(calculation_solved.calculation, calculation_solved.result)

            # Execute the query
            async with self.db as conn:
                await conn.execute(query)

        except Exception:
            raise APIException(status_code=500, message="Internal server error")

    async def list(self) -> Optional[AllCalculations]:
        try:
            calculation_results = Table('calculation_results')
            query = Query.from_(calculation_results).select(calculation_results.expression, calculation_results.result)

            async with self.db as conn:
                result = await conn.fetch_all(query)

            return TypeAdapter(AllCalculations).validate_python(result)

        except Exception:
            raise APIException(status_code=500, message="Internal server error")

