import http
from typing import Optional
from fastapi import HTTPException


class InvalidExpressionToCalculate(HTTPException):
    """Specific exception for invalid expression to calculate"""


class APIException(Exception):
    def __init__(self, status_code: int, message: Optional[str] = None, headers: Optional[dict] = None) -> None:
        self.status_code = status_code
        self.headers = headers
        if message is None:
            message = http.HTTPStatus(status_code).phrase
        self.message = message
