"""List of all constants used in the API"""

ALLOWED_OPERATORS = ["+", "-", "/", "*"]
DB_CONFIG = {
    'user': 'test',
    'password': 'test',
    'host': 'db',
    'port': '3306',
    'database': 'your_database',
}
DB_POOL_TIMEOUT = 600
DATABASE_POOL_SIZE = 5
