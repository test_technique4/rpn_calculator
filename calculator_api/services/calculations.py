from typing import Optional
from calculator_api.services.base import AbstractCalculationServices
from calculator_api.models.calculation_models import AllCalculations, CalculationSolved
from calculator_api.repository.calculations import CalculationRepository as calculation_repository
from calculator_api.domain.entities import Calculation
from calculator_api.exceptions import APIException


class CalculationServices(AbstractCalculationServices):
    """Calculation services"""

    @staticmethod
    async def insert(expression: Calculation) -> int:
        stack = []
        operators = {'+', '-', '*', '/'}

        for token in expression.split():
            if token.isdigit() or (token[0] == '-' and token[1:].isdigit()):
                stack.append(float(token))
            elif token in operators:
                if len(stack) < 2:
                    raise APIException(status_code=400, message="Invalid RPN expression")
                operand2 = stack.pop()
                operand1 = stack.pop()
                if token == '+':
                    result = operand1 + operand2
                elif token == '-':
                    result = operand1 - operand2
                elif token == '*':
                    result = operand1 * operand2
                elif token == '/':
                    if operand2 == 0:
                        raise APIException(status_code=400, message="Division by zero")
                    result = operand1 / operand2
                stack.append(result)

        if len(stack) != 1:
            raise APIException(status_code=400, message="Invalid RPN expression")

        entry = CalculationSolved(calculation=expression, result=stack[0])
        await calculation_repository.insert(entry)

        return stack[0]

    @staticmethod
    async def list() -> Optional[AllCalculations]:
        return await calculation_repository.list()

