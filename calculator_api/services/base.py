from abc import ABC, abstractmethod
from typing import Optional
from calculator_api.domain.entities import Calculation
from calculator_api.models.calculation_models import AllCalculations


class AbstractCalculationServices(ABC):
    """Calculation services interface"""

    @staticmethod
    @abstractmethod
    async def insert(expression: Calculation) -> int:
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    async def list() -> Optional[AllCalculations]:
        raise NotImplementedError
