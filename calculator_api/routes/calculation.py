from dependency_injector.wiring import Provide, inject
from fastapi import APIRouter, Depends
from typing import Optional
from abc import ABC
from calculator_api.domain.entities import Calculation
from calculator_api.dependency_injection import Container
from calculator_api.models.calculation_models import Result, AllCalculations
from calculator_api.exceptions import APIException
from calculator_api.services.base import AbstractCalculationServices

router = APIRouter(prefix="/calculation", tags=["Calculation"])


@router.post("/solve", status_code=201, response_model=Result)
@inject
async def calculate(calculation: Calculation,
                    service: AbstractCalculationServices = Depends(Provide[Container.calculation_service])) -> Result:
    """Solve the given calculation and store it and the result in the database"""
    try:
        result = await service.insert(calculation)

        return Result(result=result)
    except Exception as e:
        raise APIException(status_code=500, message="Unexpected api error") from e


@router.get("/list", status_code=200, response_model=AllCalculations)
@inject
async def list_calculations(service: AbstractCalculationServices = Depends(ABC)) -> Optional[AllCalculations]:
    """List all the calculations stored in the database and their results"""
    try:
        output = await service.list()
        return output
    except Exception as e:
        raise APIException(status_code=500, message="Unexpected api error") from e
