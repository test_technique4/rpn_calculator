import pytest
from calculator_api.domain.entities import Calculation


class TestEntities:
    def test_raises_no_exception_when_expression_is_ok(self):
        test: str = "2 10 7 25 - + *"
        Calculation(calculation=test)

    def test_raises_exception_when_expression_is_not_correct(self):
        test: str = "3 [ 10 25 - +"
        with pytest.raises(Exception) as e:
            Calculation(calculation=test)

        assert e.value.detail == "Invalid calculation: all elements must be integers or operators (+, -, *, /)"
        assert e.value.status_code == 400
