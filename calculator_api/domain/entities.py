from pydantic import BaseModel, field_validator
from calculator_api.constants import ALLOWED_OPERATORS
from calculator_api.exceptions import InvalidExpressionToCalculate


class Calculation(BaseModel):
    calculation: str

    @field_validator("calculation")
    def calculation_validation(cls, v: str):
        for element in v.split():
            if not element or (element not in ALLOWED_OPERATORS and
                               element.isdigit() is False):
                raise InvalidExpressionToCalculate(status_code=400, detail="Invalid calculation: all elements "
                                                                           "must be integers or operators (+, -, *, /)")
