from pydantic import BaseModel
from typing import Optional


class Result(BaseModel):
    result: int


class CalculationSolved(BaseModel):
    calculation: str
    result: int


class AllCalculations(BaseModel):
    data: Optional[list[CalculationSolved]]
